window.onload = init;

//Using global namespace to have separate functions
var g = {};

function init(){
    g.preloadedImages = [];
    g.title = document.getElementById("headertitle");
    g.body = document.getElementsByTagName("body")[0];
    g.gamecontainer = document.getElementById("gamecontainer");
    g.gameWindow = document.getElementById("gamebackground");
    g.instructions = document.getElementById("instructions");
    g.footertext = document.getElementById("footertext");
    g.tiles = document.getElementsByClassName("tile");
    g.languageRequest;
    g.imagesRequest;
    var englishBtn = document.getElementById("english");
    var frenchBtn = document.getElementById("francais");
    englishBtn.addEventListener("click", changeLanguage);
    frenchBtn.addEventListener("click", changeLanguage);
    g.movesCount = document.getElementById("moves");
    var newGameBtn = document.getElementById("newgame");
    newGameBtn.addEventListener("click", newGame);
    var endGameBtn = document.getElementById("endgame");
    endGameBtn.addEventListener("click", endGame);
    var animalsBtn = document.getElementById("animals");
    animalsBtn.addEventListener("click", setCategory);
    var heroesBtn = document.getElementById("heroes");
    heroesBtn.addEventListener("click", setCategory);
    var landscapesBtn = document.getElementById("landscapes");
    landscapesBtn.addEventListener("click", setCategory);
    var gamesBtn = document.getElementById("games");
    gamesBtn.addEventListener("click", setCategory);
    var saveBtn = document.getElementById("savegame");
    saveBtn.addEventListener("click", saveGame);
    var loadBtn = document.getElementById("loadgame");
    loadBtn.addEventListener("click", loadGame);
    g.currentTime = document.getElementById("time");
    g.playing = false;
    g.timer;

    //Array that will hold the solved tile positions in the puzzle grid
    g.solvedPuzzleTiles = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0];
    //Display the lowest number of tries
    if(localStorage.getItem("lowestTries") === null){
        document.getElementById("lowestmoves").innerHTML = 0;
        localStorage.setItem("lowestTries", 100000000);
    }
    else{
        if(localStorage.getItem("lowestTries") == 100000000){
            document.getElementById("lowestmoves").innerHTML = 0;
        }
        else{
            document.getElementById("lowestmoves").innerHTML = localStorage.getItem("lowestTries");
        }
    }
    //Display the lowest time played
    if(localStorage.getItem("lowestTime") === null){
        document.getElementById("highscore").innerHTML = "00:00:00";
        localStorage.setItem("lowestTime", "99:59:59");
    }
    else{
        if(localStorage.getItem("lowestTime") == "99:59:59"){
            document.getElementById("highscore").innerHTML = "00:00:00";
        }
        else{
            document.getElementById("highscore").innerHTML = localStorage.getItem("lowestTime");
        }
    }
    g.imagesToPreload = new Array(22);
    g.imagesToPreload[0] = "background.jpg";
    g.imagesToPreload[21] = "headerbg.jpg";
    loadImages();
    preload(g.imagesToPreload);
    g.currentImage = g.preloadedImages[1].src;
    g.body.style.backgroundImage = "url("+g.preloadedImages[0].src+")";
    g.body.style.backgroundSize = "1920px";
    g.body.style.backgroundPositionY = "-320px";
    setTimeout(loadPageAnimation, 100);
    g.gameWindow.style.backgroundImage = "url("+g.currentImage+")";
    document.getElementsByTagName("footer")[0].style.backgroundImage = "url("+g.preloadedImages[21].src+")";
    document.getElementsByTagName("header")[0].style.backgroundImage = "url("+g.preloadedImages[21].src+")";
    setTileProportions();
    var tilesArr = document.getElementsByClassName("tile");
    for(var i =0; i<tilesArr.length; i++){
        tilesArr[i].addEventListener("click", moveTiles);
    }
    //Check if cookie attemptCount exists
    if(document.cookie.indexOf("attemptCount=") == -1){
        //If not, display welcome message and create cookie + increment
        footer.innerHTML="Welcome new user!";
        setCookie(0);
    }
    attempts.innerHTML = getCookie();
}

//Function that animates the UI elements when first loading the page
function loadPageAnimation(){
    g.title.style.fontSize = "50px";
    setTimeout(function(){g.instructions.style.marginLeft="0px";g.footertext.style.marginLeft = "0px";}, 600)
    g.gamewidth = g.gameWindow.clientHeight;
    g.gameWindow.style.width = g.gamewidth+"px"; 
    g.gameWindow.style.height = g.gamewidth+"px";
}

//Function that preloads the images whose src are passed as arguments
function preload(images) {
    for (var i = 0; i < g.imagesToPreload.length; i++) {
        g.preloadedImages[i] = new Image();
        g.preloadedImages[i].src = images[i];
    }
}

//Function that starts the timer
function startTimer(){
    var hours = 0;
    var minutes = 0;
    var seconds = 0;
    //Construct the time string
    var timeString = "";
    var hoursModifier = "";
    var minutesModifier = "";
    var secondsModifier = "";
    g.currentTime.innerHTML = "00:00:00";
    g.timer = setInterval(function(){
        seconds += 1;
        if(hours < 10){
            hoursModifier = "0";
        }
        else{
            hoursModifier = "";
        }
        if(minutes < 10){
            minutesModifier = "0";
        }
        else{
            minutesModifier = "";
        }
        if(seconds < 10){
            secondsModifier = "0";
        }
        else{
            secondsModifier = "";
        }
        timeString = hoursModifier+ hours+":"+minutesModifier+minutes+":"+secondsModifier+seconds;
        g.currentTime.innerHTML = timeString;
        if(seconds == 59){
            minutes++;
            seconds = 0;
        }
        if(minutes == 59){
            minutes = 0;
            hours++;
        }

    }, 1000);
}

//Function that stops the timer
function stopTimer(){
    clearInterval(g.timer);
}

//Handler for category buttons that randomizes images based on selection
function setCategory(e){
    if(g.playing == true){
        alert("Cannot change picture mid game! Please end the current game first");
    }
    else{
        var category = e.target.id;
        var randomImgIndex = 0;
        //If the category is animals generate a rand number from 1 to 5
        if(category == "animals"){
            randomImgIndex = Math.floor(Math.random() * (6 - 1)) + 1;
        }
        //If the category is super heroes generate a rand number from 6 to 10
        else if(category == "heroes"){
            randomImgIndex = Math.floor(Math.random() * (11 - 6)) + 6;
        }
        //If the category is beautiful landscapes generate a rand number from 11 to 15
        else if(category == "landscapes"){
            randomImgIndex = Math.floor(Math.random() * (16 - 11)) + 11;
        }
        //If the category is video games generate a rand number from 16 to 20
        else{
            randomImgIndex = Math.floor(Math.random() * (21 - 16)) + 16;
        }
        g.currentImage = g.preloadedImages[randomImgIndex].src;

        g.gameWindow.style.backgroundImage = "url("+g.currentImage+")";
    }
}

//Set a cookies value
function setCookie(count){
    //Set up the expiry date
    var fivehoursfromnow = new Date();
    fivehoursfromnow.setHours(fivehoursfromnow.getHours + 5);
    var expires = "; expires=" + fivehoursfromnow.toGMTString();
    //Set up the cookie name and value pair for the count of visits
    var cookieString = "attemptCount=" + count + expires + ";";
    document.cookie = cookieString;
}

function getCookie(){
    var cookie = document.cookie;
    var cookievalue = cookie.substring(cookie.indexOf('=')+1);
    return parseInt(cookievalue);
}

//Retrieve a text file using AJAX
function createRequest(){
    if(window.XMLHttpRequest){
        var request = new XMLHttpRequest();
    }
    else{
        request = new ActiveXObject("Microsoft.XMLHTTP");
    }
    return request;
}

//Process language AJAX request
function processLanguageRequest(request, language){
    request.onreadystatechange = displayLanguage;
    request.open("GET", language + ".txt", false);
    request.send(null);
}

//Process the images AJAX request
function processImagesRequest(request){
    request.onreadystatechange = imageLoading;
    request.open("GET", "images.txt", false);
    request.send(null);
}

//Change the language
function changeLanguage(e){
    var target = e.target;
    var language = target.id;
    g.languageRequest = createRequest();
    processLanguageRequest(g.languageRequest, language);
}

//Create the request to load the images
function loadImages(){
    g.imagesRequest = createRequest();
    processImagesRequest(g.imagesRequest);
}

//Load the images
function imageLoading(){
    if(g.imagesRequest.readyState == 4 && g.imagesRequest.status == 200){
        var imagesSrc = g.imagesRequest.responseText.split(",");
        var j = 0;
        for(var i = 1; i < imagesSrc.length + 1; i++){
            g.imagesToPreload[i] = imagesSrc[j];
            j++;
        }
    }
}

//Display the language
function displayLanguage(){
    if (g.languageRequest.readyState == 4 && g.languageRequest.status == 200){
        var contents = g.languageRequest.responseText.split(",");
        var title = document.getElementsByTagName("h1")[0];
        var instructions = document.getElementsByTagName("h2")[0];
        var buttons = document.getElementsByTagName("button");
        var labels = document.getElementsByTagName("label");
        //Change the language of the title
        title.innerHTML = contents[0];
        //Change the language of the instructions
        instructions.innerHTML = contents[1];
        //Change the language of the labels
        var counter = 0;
        var numLabels = labels.length + 2;
        for (var i=2; i<numLabels; i++){
            labels[counter].innerText = contents[i];
            counter++;
        }
        //Change the language of the buttons
        counter = 0;
        for (var j = numLabels; j< (buttons.length + numLabels); j++){
            buttons[counter].innerHTML = contents[j];
            counter++;
        }
    }
}

//Function that sets the proportions of the tiles before loading them
function setTileProportions(){
    //Calculate the width and height for each tile
    var length = g.gameWindow.clientHeight / 4;
    for(var i = 0; i < g.tiles.length; i++){
        g.tiles[i].style.width = length + "px";
        g.tiles[i].style.height = length + "px";
    }
}

function newGame(){
    if(!g.playing){
        g.playing = true;
        g.tilePositions = shuffleTiles();
        displayImageArray(g.tilePositions);
        document.getElementById("moves").innerHTML = "0";
        var attempts = document.getElementById("attempts");
        var attemptCount = parseInt(attempts.innerHTML);
        setCookie(attemptCount +1);
        attempts.innerHTML = getCookie();
        startTimer();
    }
    else{
        alert("Please end the game first before starting a new one");
    }
}

function endGame(){
    g.playing = false;
    document.getElementById("moves").innerHTML = "0";
    var array = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1];
    displayImageArray(array);
    stopTimer();
}

function saveGame(){
    if(g.playing == false){
        alert("Cannot save game - please start a game to be able to save it");
    }
    else{
        var saveDataString = "";
        saveDataString += g.movesCount.innerHTML + ",";
        for(var i=0; i<g.tilePositions.length; i++){
            saveDataString += g.tilePositions[i] + ",";
        }
        var imageURL = g.tiles[0].style.backgroundImage;
        imageURL = imageURL.substring(5,(imageURL.indexOf(")")-1));
        saveDataString += imageURL +",";
        saveDataString += g.currentTime.innerHTML;
        localStorage.setItem("saved", saveDataString);
    }
}

function loadGame(){
    if(!g.playing){
        if(localStorage.getItem("saved") === null){
            alert("No previous save data was found");
        }
        else{
            g.playing = true;
            var savedData = localStorage.getItem("saved").split(",");
            g.movesCount.innerHTML = savedData[0];
            var tileIndex = 0;
            g.tilePositions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0];
            for(var i = 1; i< g.tilePositions.length +1; i++){
                g.tilePositions[tileIndex] = savedData[i];
                tileIndex++;
            }
            g.currentImage = savedData[g.tilePositions.length+1];
            g.gameWindow.style.backgroundImage = "url("+g.currentImage+")";
            displayImageArray(g.tilePositions);
            //Keep the timer going
            g.currentTime.innerHTML = savedData[g.tilePositions.length+2];
            var seconds = 0;
            var minutes = 0;
            var hours = 0;
            if(parseInt(g.currentTime.innerHTML.substring(6,7)) == 0){
                seconds = parseInt(g.currentTime.innerHTML.substring(7,8));
            }
            else{
                seconds = parseInt(g.currentTime.innerHTML.substring(6,8));
            }
            if(parseInt(g.currentTime.innerHTML.substring(3,4)) == 0){
                minutes = parseInt(g.currentTime.innerHTML.substring(4,5));
            }
            else{
                minutes = parseInt(g.currentTime.innerHTML.substring(3,5));
            }
            if(parseInt(g.currentTime.innerHTML.substring(0,1)) == 0){
                hours = parseInt(g.currentTime.innerHTML.substring(1,2));
            }
            else{
                hours = parseInt(g.currentTime.innerHTML.substring(0,2));
            }
            var timeString = "";
            var hoursModifier = "";
            var minutesModifier = "";
            var secondsModifier = "";
            g.timer = setInterval(function(){
                seconds += 1;
                if(hours < 10){
                    hoursModifier = "0";
                }
                else{
                    hoursModifier = "";
                }
                if(minutes < 10){
                    minutesModifier = "0";
                }
                else{
                    minutesModifier = "";
                }
                if(seconds < 10){
                    secondsModifier = "0";
                }
                else{
                    secondsModifier = "";
                }
                timeString = hoursModifier+ hours+":"+minutesModifier+minutes+":"+secondsModifier+seconds;
                g.currentTime.innerHTML = timeString;
                if(seconds == 59){
                    minutes++;
                    seconds = 0;
                }
                if(minutes == 59){
                    minutes = 0;
                    hours++;
                }

            }, 1000);
        }
    }
    else{
        alert("Please end the current game first before loading another one");
    }
}

//Function that randomizes the tiles while making sure the puzzle is solvable
function shuffleTiles(){
    var array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    var solvable = false;
    while(solvable == false){
        for(var i = 0; i < array.length - 1; i++){
            array[i] = generateUniqueRandomPosition(array);
        }
        if(isSolvable(array)){
            solvable = true;
        }
        else{
            array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        }
    }
    return array;
}

//Helper function that checks if a given puzzle is solvable
function isSolvable(array){
    var inv_count = inversionCount(array);
    if(inv_count % 2 == 0){
        return true;
    }
    else{
        return false;
    }
}

function inversionCount(array){
    var count = 0;
    for(var i=0; i < 15; i++){
        for(var j=i+1; j < 16; j++){
            if(array[j] && array[i] && array[i] > array[j]){
                count++;
            }
        }
    }
    return count;
}

//Helper function that generates a unique random number representing the tile position from 1-15
function generateUniqueRandomPosition(array){
    var placed = "false";
    var random = 0;
    while(placed == "false"){
        random = Math.floor(Math.random() * (16 - 1)) + 1;
        for(var i = 0; i < array.length; i++){
            if(array[i] == random){
                break;
            }
            if(i == array.length -1){
                if(array[i] != random){
                    placed = "true";
                }
            }
        }
    }
    return random;
}
//Moves the tiles around as the user plays the game
function moveTiles(e){
    if(g.playing){
        var clicked = parseInt(e.target.id) -1;
        //Get the empty tile position
        var emptyTilePosition = -1;
        for(var i = 0; i<g.tilePositions.length; i++){
            if(g.tilePositions[i] == 0){
                emptyTilePosition = i;
            }
        }
        //Check if the tile can be moved
        if(emptyTilePosition == (clicked + 1) || emptyTilePosition == (clicked - 1) || emptyTilePosition == (clicked + 4) || emptyTilePosition == (clicked - 4)){
            //Exchange 2 positions
            g.tilePositions[emptyTilePosition] = g.tilePositions[clicked];
            g.tilePositions[clicked] = 0;
            //Draw the board with the new positions
            displayImageArray(g.tilePositions);
            var hasSolvedPuzzle = true;
            for(var j = 0; j < g.tilePositions.length; j++){
                if(g.tilePositions[j] != g.solvedPuzzleTiles[j]){
                    hasSolvedPuzzle = false;
                    break;
                }
            }
            if(hasSolvedPuzzle){
                g.playing = false;
                alert("Congratulations! You won");
                //Set the lowest number of moves in localstorage if it is the lowest
                var currentMoves = parseInt(g.movesCount.innerHTML) +1;
                if(currentMoves < localStorage.getItem("lowestTries")){
                    localStorage.setItem("lowestTries", currentMoves);
                    document.getElementById("lowestmoves").innerHTML = localStorage.getItem("lowestTries");
                }
                //Get the current lowest time and compare - update highscore field if new time is lower
                stopTimer();
                var currentSeconds = 0;
                var currentMinutes = 0;
                var currentHours = 0;
                if(parseInt(g.currentTime.innerHTML.substring(6,7)) == 0){
                    currentSeconds = parseInt(g.currentTime.innerHTML.substring(7,8));
                }
                else{
                    currentSeconds = parseInt(g.currentTime.innerHTML.substring(6,8));
                }
                if(parseInt(g.currentTime.innerHTML.substring(3,4)) == 0){
                    currentMinutes = parseInt(g.currentTime.innerHTML.substring(4,5));
                }
                else{
                    currentMinutes = parseInt(g.currentTime.innerHTML.substring(3,5));
                }
                if(parseInt(g.currentTime.innerHTML.substring(0,1)) == 0){
                    currentHours = parseInt(g.currentTime.innerHTML.substring(1,2));
                }
                else{
                    currentHours = parseInt(g.currentTime.innerHTML.substring(0,2));
                }
                //Get the highscore values
                var highscoreSeconds = 0;
                var highscoreMinutes = 0;
                var highscoreHours = 0;
                var highscoreString = localStorage.getItem("lowestTime");
                if(parseInt(highscoreString.substring(6,7)) == 0){
                    highscoreSeconds = parseInt(highscoreString.substring(7,8));
                }
                else{
                    highscoreSeconds = parseInt(highscoreString.substring(6,8));
                }
                if(parseInt(highscoreString.substring(3,4)) == 0){
                    highscoreMinutes = parseInt(highscoreString.substring(4,5));
                }
                else{
                    highscoreMinutes = parseInt(highscoreString.substring(3,5));
                }
                if(parseInt(highscoreString.substring(0,1)) == 0){
                    highscoreHours = parseInt(highscoreString.substring(1,2));
                }
                else{
                    highscoreHours = parseInt(highscoreString.substring(0,2));
                }
                //Calculate the total number of hours for the 2 times to compare more easily
                var highscore = highscoreHours + (highscoreMinutes /60) + (highscoreSeconds /3600);
                var currentScore = currentHours + (currentMinutes /60) + (currentSeconds /3600);
                if(currentScore < highscore){
                    alert("New time record set!");
                    //Save the current highscore in localstorage
                    localStorage.setItem("lowestTime", g.currentTime.innerHTML);
                    document.getElementById("highscore").innerHTML = localStorage.getItem("lowestTime");
                }
            }
            else{
                incrementMoves();
            }
        }
        else{
            //Can't move tile
        }
    }
    else{
        alert("Cannot move tiles until game has started");
    }
}

function incrementMoves(){
    var currentMoves = parseInt(g.movesCount.innerHTML);
    g.movesCount.innerHTML = currentMoves + 1;
}

//Displays the game image using the array with positions
function displayImageArray(array){
    for(var i = 0; i < g.tiles.length; i++){
        if(array[i] == 0){
            g.tiles[i].style.backgroundImage = "";
            g.tiles[i].style.backgroundColor = "white";
            g.tiles[i].style.opacity = "0.7";
        }
        else if(array[i] == -1){
            g.tiles[i].style.backgroundImage = "";
            g.tiles[i].style.backgroundColor = "";
            g.tiles[i].style.opacity = "1";
        }
        else{
            g.tiles[i].style.opacity = "1";
            g.tiles[i].style.backgroundImage = "url("+g.currentImage+")";
            g.tiles[i].style.backgroundSize = g.gameWindow.clientHeight + "px";
            if(array[i] <= 4){
                g.tiles[i].style.backgroundPosition = determineBackgroundPositioning(0, (array[i] - 1));
            }
            else if(array[i] <= 8){
                g.tiles[i].style.backgroundPosition = determineBackgroundPositioning(1, ((array[i]-4) - 1));
            }
            else if(array[i] <= 12){
                g.tiles[i].style.backgroundPosition = determineBackgroundPositioning(2, ((array[i]-8) - 1));
            }
            else{
                g.tiles[i].style.backgroundPosition = determineBackgroundPositioning(3, ((array[i]-12) - 1));
            }
        }
    }  
}   

//Helper function for displayImageArray - provides the backPosition style string
function determineBackgroundPositioning(row, col){
    var distance = g.gameWindow.clientHeight / 4;
    var distanceToMoveFromLeft = 0;
    var distanceToMoveFromTop = 0;
    var backgroundPosition = "";
    //Determine the x positioning
    if(col == 0){
        //Already set to 0
    }
    else if(col == 1){
        distanceToMoveFromLeft -= distance;
    }
    else if(col == 2){
        distanceToMoveFromLeft -= (distance * 2);
    }
    else{
        distanceToMoveFromLeft -= (distance * 3);
    }

    //Determine the y positioning
    if(row == 0){
        //Already set to 0
    }
    else if(row == 1){
        distanceToMoveFromTop += distance *3;
    }
    else if(row == 2){
        distanceToMoveFromTop += (distance *2);
    }
    else{
        distanceToMoveFromTop += (distance * 1);
    }

    backgroundPosition = distanceToMoveFromLeft+"px "+distanceToMoveFromTop+"px";
    return backgroundPosition;
}
